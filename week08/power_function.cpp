#include <iostream>


//POST: returns the result of x^n.
unsigned int power (const int x, const unsigned int n) {
  
  //base case
  if (n == 0) {
    return 1;
  } 
  
  //recursive case
  
  //Assume a correct result of pow(x, n/2)
  unsigned int prev = power(x, n/2);
  
  //Calculate pow(x, n) using the result of pow(x, n/2)
  unsigned int result = prev * prev;
  if (n % 2 == 1) result *= x;
  
  return result;  
}

int main() {
  int x;
  unsigned int n;
  
  std::cin >> x >> n;
  
  unsigned int p = power(x, n);
  
  std::cout << p << std::endl;
  
  return 0;
}