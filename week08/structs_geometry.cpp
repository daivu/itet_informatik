#include <iostream>

/* type that represents a vector in 3d-space */
struct vec {
  double x;
  double y;
  double z;
};

/* type that represents a line in 3d-space 
   Condition: direction != {0, 0, 0} */
struct line {
  vec start;
  vec direction;
};

/* POST: Returns a vector that results from the addition of two vectors  */
vec add(vec first, vec second) {
  vec result;
  result.x = first.x + second.x;
  result.y = first.y + second.y;
  result.z = first.z + second.z;
  
  /*
  result = {first.x + second.x, first.y + second.y, first.z + second.z};
  */
  return result;
}

/* POST: Shifts the line by given translation vector */
line shift(line originalLine, vec translation) {
  line resultLine;
  resultLine.start = add(originalLine.start, translation);
  resultLine.direction = originalLine.direction;
  return resultLine;
}

int main () {
  vec v1 = {1,1,1};
  vec v2 = {2,3,4};
  
  //testing vector addition
  vec addition = add(v1,v2);
  std::cout << addition.x << " " << addition.y << " " << addition.z << std::endl;
  
  
  //testing line translation
  line l1;
  l1.direction = v1;
  l1.start = v2;
  
  line result = shift(l1, v2);
  std::cout << result.start.x << " " << result.start.y << " " << result.start.z << std::endl;
}