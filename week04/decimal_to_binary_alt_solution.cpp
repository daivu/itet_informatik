#include <iostream>

using namespace std;

int main() {
  unsigned int n;
  cin >> n;

  //calculate maximum power of 2 that is smaller than n
  unsigned int power = 1; //this accomodates for the 0-case
  for (;power <= n / 2; power *= 2);
  
  //print binary number bit by bit
  for (;power > 0; power /= 2) 
    cout << (n / power) % 2;
  
  return 0;
}