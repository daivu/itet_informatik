// Program: strangesum.cpp

#include <iostream>

int main () {

  // input
  unsigned int strangesum = 0;
  unsigned int n;
  std::cin >> n;
  
  for (int i = 1; i <=n; i += 2) {
    if (i % 5 != 0) {
      strangesum = strangesum + i;
    }
  }
  
  // output
  std::cout << strangesum << "\n";

  return 0;
}
