#include <iostream>
#include <cassert>

int main () {
  unsigned int n;
  std::cin >> n;

  assert(n >= 1);
  
  unsigned int p = 1;
  
  //keep doubling p until "next" iteration would be too big
  for (;p <= n/2; p *= 2) ;
  
  std::cout << p << std::endl;
  
  return 0;
}