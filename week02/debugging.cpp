#include <iostream>
#include <cassert>

int main () {
    const int n = 6;

    // Compute n^12
    int prod = 1;

    std::cerr << "Arrived 1" << std::endl;

    for (int i = 1; 1 <= i && i < 13; ++i) {
        assert(prod <= INT32_MAX/n);
        prod *= n;
    }

    std::cerr << "Arrived 2" << std::endl;

    std::cerr << prod << std::endl;
    // Output stars

    for (int i = 1; i < prod; ++i) {
        std::cout << "*";
    }

    std::cerr << "Arrived 3" << std::endl;

    std::cout << "\n";
    return 0;
}
