#include <iostream>
#include "perfect.h"

int count_perfect(unsigned int a, unsigned int b);

int main() {
  
  //input
  unsigned int a,b;
  
  std::cin >> a >> b;
  
  //solve
  int result = count_perfect(a, b);
  
  //output
  std::cout << result;
  
  return 0;
}


//pre-conditions: a,b>0
//post-conditions: counts the number of perfect numbers in the interval [a,b]
int count_perfect(unsigned int a, unsigned int b) {
  unsigned int n=0;
  for (int i = a; i <= b; i++) {
    n += is_perfect(i);
  }
  return n;
}
