#include <iostream>
#include <string>

//POST: prints the steps needed to move n elements from stack src to stack dst
void move(int n, const std::string &src, const std::string &aux, const std::string &dst) {
  if (n == 1) {
    //base case - print
    std::cout << src << " --> " << dst << std::endl;
  } else {
    //recursive case
    
    //move the upper n-1 elements to auxiliary (recursively)
    move(n-1, src, dst, aux); 
    //move the bottom element to destination
    move(1, src, aux, dst); 
    //move n-1 elements from auxiliary to destination (recursively)
    move(n-1, aux, src, dst);
  }
}

int main() {
  int n;
  std::cin >> n;
  move(n, "left" , "middle", "right" );
  return 0;
}