#include <iostream>
#include <vector>


//POST: prints the input vector in the required format
void printPermutation(std::vector<int> &input) {
  for (int i = 0; i < input.size(); i++) {
    std::cout << input[i] << " ";
  }
  std::cout << std::endl;
}


//POST: return true if and only if the given vector contains the given element
bool contains(std::vector<int> &input, int element) {
  for (int i = 0; i < input.size(); i++) {
    if (input[i] == element) return true;
  }
  return false;
}


//POST: Prints all permutations of a given vector
void generatePermutations(std::vector<int> permutation, std::vector<int> &sequence) {
  if (permutation.size() == sequence.size()) {
    //base case
    printPermutation(permutation);
  } else {
    
    //recursive case
    
    /* 
      for each element not yet contained in the permutation vector of length n,
      add that element to the back to generate a new permutation of length n+1.
    */
    for (int i = 0; i < sequence.size(); i++) {
      if (!contains(permutation, sequence[i])) {
        //generate a new permutation
        permutation.push_back(sequence[i]);
        //generate further permutations based on current permutation
        generatePermutations(permutation, sequence);
        //backtrack, in order to try a different permutation
        permutation.pop_back();
      }
    }
    
    }
}


int main () {
  std::vector<int> sequence;
  
  // Read input.
  int num;
  std::cin >> num;
  while (num != -1) {
    sequence.push_back(num);
    std::cin >> num;
  }

  //empty vector, from which permutations are generated
  std::vector<int> emptyVector;
  
  //call recursive function
  generatePermutations(emptyVector, sequence);
  
  return 0;
}