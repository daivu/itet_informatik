#include <iostream>
#include <vector>
#include <cassert>

using irow = std::vector<int>;
using imatrix = std::vector<irow>;

// POST: Returns the number of rows.
unsigned int get_rows(const imatrix &matrix);
// POST: Returns the number of columns.
unsigned int get_cols(const imatrix &matrix);

// POST: Return a matrix that is a transpose of the input matrix.
imatrix transpose_matrix(const imatrix &matrix) {
  unsigned int r, c;
  r = get_rows(matrix);
  c = get_cols(matrix);
  imatrix transposed_matrix;


  for (int j = 0; j < c; j++) {
    irow row;
    //read column j into a vector
    for (int i = 0; i < r; i++) {
      row.push_back(matrix[i][j]);
    }
    
    //push column j from original matrix into transposed matrix as a row
    transposed_matrix.push_back(row);
    
  }

  return transposed_matrix;
}


unsigned int get_rows(const imatrix &matrix) {
  return matrix.size();
}

// POST: Returns the number of columns.
unsigned int get_cols(const imatrix &matrix) {
  if (get_rows(matrix) > 0) {
    return matrix[0].size();
  }
  // Empty matrix has no columns.
  return 0;
}

// POST: Returns a matrix that was read from the standard input.
imatrix read_matrix() {
  unsigned int r, c;
  std::cin >> r >> c;
  imatrix matrix;
  for (unsigned int row_index = 0; row_index < r; row_index++) {
    irow row;
    for (unsigned int col_index = 0; col_index < c; col_index++) {
      int element;
      std::cin >> element;
      row.push_back(element);
    }
    matrix.push_back(row);
  }
  return matrix;
}

// POST: Writes the matrix into standard output.
void write_matrix(const imatrix &matrix) {
  unsigned int r, c;
  r = get_rows(matrix);
  c = get_cols(matrix);
  std::cout << r << " " << c << std::endl;
  for (unsigned int row_index = 0; row_index < r; row_index++) {
    for (unsigned int col_index = 0; col_index < c; col_index++) {
      std::cout << matrix[row_index][col_index] << " ";
    }
    std::cout << std::endl;
  }
}

int main () {
  imatrix matrix = read_matrix();
  imatrix transposed_matrix = transpose_matrix(matrix);
  write_matrix(transposed_matrix);
  return 0;
}