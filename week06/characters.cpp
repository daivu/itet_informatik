#include <iostream>
#include <vector>


void vectorToUpper(std::vector<char>& input);
void characterToUpper(char& input);

int main () {
  // Say to std::cin not to ignore whitespace.
  std::cin >> std::noskipws;

  //input
  char c;
  std::vector<char> letters;
  
  do {
    std::cin >> c;
    letters.push_back(c);
  } while (c != '\n');
  
  //compute
  vectorToUpper(letters);
  
  //print
  for (int i = 0; i < letters.size(); i++) {
    std::cout << letters[i];
  }
}

//pre-condition: none
//post-condition: all lower case letters converted to upper
void vectorToUpper(std::vector<char>& input) {
  for (int i = 0; i < input.size(); i++) {
    characterToUpper(input[i]);
  }
}

//pre-condition: none
//post-condition:: lower case letter to upper
void characterToUpper(char& input) {
  if (input >= 'a' && input <= 'z') {
    input += 'A' - 'a';
  }
}