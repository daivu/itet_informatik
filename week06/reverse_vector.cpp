#include <iostream>
#include <vector>

void reverse(std::vector<int>& input);
void swap(std::vector<int>& input, int frontIndex, int backIndex);

int main() {
  //input
  std::vector<int> v;
  int n = 0;
  
  std::cin >> n;
  
  //check that n<=10;
  if(n > 10) {
    std::cout << "Bad input";
    return 0;
  }
  
  for (int i = 0; i < n; i++) {
    int n;
    std::cin >> n;
    v.push_back(n);
  }
  
  //compute
  reverse(v);
  
  //print
  for (int i = 0; i < v.size(); i++) {
    std::cout << v[i] << " ";
  }
  
  return 0;
}

//pre-condition: none
//post-condition: changes vector elements to reversed order
void reverse(std::vector<int>& input) {
  int front = 0;
  int back = input.size() - 1;
  
  while (front < back) {
    swap(input, front, back);
    front++;
    back--;
  }
}

//pre-condition: frontIndex <= backIndex
//post-condition: swaps elements in locations frontIndex and backIndex
void swap(std::vector<int>& input, int frontIndex, int backIndex) {
  int temp = input[frontIndex];
  input[frontIndex] = input[backIndex];
  input[backIndex] = temp;
}