#include <iostream>
#include <cassert>
#include "tribool.h"

/* AND operation on many tribools */
void conjoin() {
   tribool result(2);
   tribool current(0);
   
   while (std::cin >> current) {
     result = result && current;
   }
   
   std::cout << result;
}

// Uncomment once completed constructors.
 void test_constructors() {
   tribool_false();
   tribool_unknown();
   tribool_true();
 }

// Uncommment once completed equality.
 void test_equality() {
   tribool tfalse = tribool_false();
   tribool tunknown = tribool_unknown();
   tribool ttrue = tribool_true();
  
   assert(tfalse == tfalse);
   assert(!(tfalse == tunknown));
   assert(!(tfalse == ttrue));
  
   assert(!(tunknown == tfalse));
   assert(tunknown == tunknown);
   assert(!(tunknown == ttrue));
  
   assert(!(ttrue == tfalse));
   assert(!(ttrue == tunknown));
   assert(ttrue == ttrue);
 }

// Uncomment once completed and.
 void test_and() {
   tribool tfalse = tribool_false();
   tribool tunknown = tribool_unknown();
   tribool ttrue = tribool_true();
  
   assert((tfalse && tfalse) == tfalse);
   assert((tfalse && tunknown) == tfalse);
   assert((tfalse && ttrue) == tfalse);
   assert((tunknown && tfalse) == tfalse);
   assert((tunknown && tunknown) == tunknown);
   assert((tunknown && ttrue) == tunknown);
   assert((ttrue && tfalse) == tfalse);
   assert((ttrue && tunknown) == tunknown);
   assert((ttrue && ttrue) == ttrue);
 }

// Uncomment once completed or.
 void test_or() {
   tribool tfalse = tribool_false();
   tribool tunknown = tribool_unknown();
   tribool ttrue = tribool_true();
  
   assert((tfalse || tfalse) == tfalse);
   assert((tfalse || tunknown) == tunknown);
   assert((tfalse || ttrue) == ttrue);
   assert((tunknown || tfalse) == tunknown);
   assert((tunknown || tunknown) == tunknown);
   assert((tunknown || ttrue) == ttrue);
   assert((ttrue || tfalse) == ttrue);
   assert((ttrue || tunknown) == ttrue);
   assert((ttrue || ttrue) == ttrue);
 }

// Uncomment once completed bool and.
 void test_bool_and() {
   tribool tfalse = tribool_false();
   tribool tunknown = tribool_unknown();
   tribool ttrue = tribool_true();
  
   assert((tfalse && true) == tfalse);
   assert((tfalse && false) == tfalse);
   assert((tunknown && true) == tunknown);
 assert((tunknown && false) == tfalse);
   assert((ttrue && true) == ttrue);
   assert((ttrue && false) == tfalse);
 }

// Uncomment once completed bool or.
 void test_bool_or() {
  tribool tfalse = tribool_false();
   tribool tunknown = tribool_unknown();
   tribool ttrue = tribool_true();
  
   assert((tfalse || true) == ttrue);
   assert((tfalse || false) == tfalse);
   assert((tunknown || true) == ttrue);
   assert((tunknown || false) == tunknown);
   assert((ttrue || true) == ttrue);
   assert((ttrue || false) == ttrue);
 }

int main () {
  test_constructors();
  test_equality();
  test_and();
  test_or();
  test_bool_and();
  test_bool_or();
  conjoin();
}
