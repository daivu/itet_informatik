/*
Tribool is three-valued logic {false, unknown, true}. 
*/

class tribool {
  private:
  
  /*
  Stores state of tribool.
  0 = false, 1 = unknown, 2 = true
  */
  unsigned int state;
  
  public:
  
  //constructor. Default value: unkown (1)
  tribool (int input);
  
  //POST: change state of this tribool
  void setValue(int input);
  
  //POST: return state of this tribool as an int
  unsigned int getValue();
  
  //operator overloading for '==' between tribools
  bool operator== (const tribool& other) const;
  
  //operator overloading for '&&' between tribools
  tribool operator&& (const tribool& other) const;
  
  //operator overloading for '||' between tribools
  tribool operator|| (const tribool& other) const;
  
  //operator overloading for '&&' between tribool and bool
  tribool operator&& (bool other) const;
  
  //operator overloading for '||' between tribool and bool
  tribool operator|| (bool other) const;
  
};

//operator overloading for '&&' between bool and tribool
tribool operator&& (bool first, const tribool& second);

//operator overloading for '||' between bool and tribool
tribool operator|| (bool first, const tribool& second);

//operator overloading for reading value into tribool
std::istream& operator>>(std::istream& is, tribool& t);

//POST: print tribool value
std::ostream& operator<<(std::ostream& os, tribool& t);

//POST: returns false tribool
tribool tribool_false();

//POST: returns unknown tribool
tribool tribool_unknown();

//POST: returns true tribool
tribool tribool_true();