#include <iostream>
#include <string>
#include "tribool.h"

//constructor. Default value: unkown (1)
tribool::tribool (int input = 1) {
  this->state = input;
}

//POST: change state of this tribool
void tribool::setValue (int input) {
  this->state = input;
}

//POST: return state of this tribool as an int
unsigned int tribool::getValue () {
  return this->state;
}

//operator overloading for '==' between tribools
bool tribool::operator== (const tribool& other) const {
  return this->state == other.state;
}

//operator overloading for '&&' between tribools
tribool tribool::operator&& (const tribool& other) const {
  tribool result(std::min(this->state, other.state));
  return result;
}

//operator overloading for '||' between tribools
tribool tribool::operator|| (const tribool& other) const {
  tribool result(std::max(this->state, other.state));
  return result;
}

//operator overloading for '&&' between tribool and bool
tribool tribool::operator&& (bool other) const {
  tribool otherTribool(other * 2);
  tribool result(std::min(this->state, otherTribool.state));
  return result;
}

//operator overloading for '||' between tribool and bool
tribool tribool::operator|| (bool other) const {
  tribool otherTribool(other * 2);
  tribool result(std::max(this->state, otherTribool.state));
  return result;
}

//operator overloading for '&&' between bool and tribool
tribool operator&& (bool first, const tribool& second) {
  return second && first;
}

//operator overloading for '||' between bool and tribool
tribool operator|| (bool first, const tribool& second) {
  return second || first;
}

//operator overloading for reading value into tribool
std::istream& operator>>(std::istream& in, tribool& t) {
  std::string value;
  if (in >> value) {
    if (value == "true") {
      t = tribool_true();
    } else if (value == "unknown") {
      t = tribool_unknown();
    } else if (value == "false") {
      t = tribool_false();
    } else {
      // Read unrecognised value.
      in.setstate(std::ios::failbit);
    }
  } else {
    // Failed to read.
    in.setstate(std::ios::failbit);
  }
  return in;
}

//POST: print tribool value
std::ostream& operator<<(std::ostream& os, tribool& t) {
  if (t.getValue() == 0) {
    os << "false";
  } else if (t.getValue() == 1) {
    os << "unknown";
  } else {
    os << "true";
  }
  
  return os;
}

//POST: returns false tribool
tribool tribool_false() {
  tribool t(0);
  return t;
}

//POST: returns unknown tribool
tribool tribool_unknown() {
  tribool t(1);
  return t;
}

//POST: returns true tribool
tribool tribool_true() {
  tribool t(2);
  return t;
}