#include <iostream>
#include <cassert>
#include "vector_linkedlist.h"

void llvec::swap(unsigned int index) {
  llnode* curr = this->head;
  
  if (index > 0) {
    for (unsigned int i = 0; i < index - 1; i++) {
      curr = curr->next;
    }
  }
  
  llnode* firstElement;
  
  if (index == 0) {
    firstElement = this->head;
  } else {
    firstElement = curr->next;
  }
  llnode* secondElement = firstElement->next;
  if (index == 0) {
    this->head = secondElement;
  } else {
    curr->next = secondElement;
  }
  firstElement->next = secondElement->next;
  secondElement->next = firstElement;
}

llvec::llvec() {
  this->head = nullptr;
}

std::ostream& operator<<(std::ostream& sink, const llvec& vec) {
  sink << '[';
  for (llvec::const_iterator it = vec.begin(); it != vec.end(); ++it) {
    sink << *it << ' ';
  }
  sink << ']';
  return sink;
}

void llvec::push_front(int e) {
  this->head = new llnode { e, this->head };
}

llvec::const_iterator llvec::begin() const {
  return llvec::const_iterator(this->head);
}

llvec::const_iterator llvec::end() const {
  return llvec::const_iterator(nullptr);
}


llvec::const_iterator::const_iterator(const llnode* const n): node(n) {}

llvec::const_iterator& llvec::const_iterator::operator++() {
  assert(this->node != nullptr);

  this->node = this->node->next;

  return *this;
}

const int& llvec::const_iterator::operator*() const {
  return this->node->value;
}

bool llvec::const_iterator::operator!=(const llvec::const_iterator& other) const {
  return this->node != other.node;
}

bool llvec::const_iterator::operator==(const llvec::const_iterator& other) const {
  return this->node == other.node;
}