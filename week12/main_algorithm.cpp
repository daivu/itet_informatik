#include <iostream>
#include <vector>
#include <algorithm>

// POST: Reads a sequence terminated by a negative number into vec.
void input(std::vector<int>& vec) {
  int e;
  std::cin >> e;
  while (e >= 0) {
    vec.push_back(e);
    std::cin >> e;
  }
}

// POST: Prints the contents of the vector into stdout.
void print(std::vector<int>& vec) {
  for (std::vector<int>::iterator it = vec.begin(); it != vec.end(); it++) {
    std::cout << *it << " ";
  }
  std::cout << std::endl;
}

int main () {

  std::vector<int> vec;
  
  input(vec);
  
  

  int largest_element = *std::max_element(vec.begin(), vec.end());
  std::cout << "The largest element in the sequence: " << largest_element << std::endl;
  
  std::sort(vec.begin(), vec.end());
  std::cout << "Sorted vector: ";
  print(vec);
}